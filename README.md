# IT Academy final test

You are given 4 services. Each of those is a Flask-based application and together they build a microservice application that is capable of adding, multiplying and displaying notes.

# Objectives
**Create one-click deployment script that will deploy this application on GCP.**

Each service is on separate container and together they create a microservice application.

Only `gateway.py` should be accesible from outside of the kubernetes network.

E.g o run service locally you can type:
```bash
python3 add.py &
```
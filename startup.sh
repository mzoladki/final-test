
#building images
python3 example_database.py
REVISION=0
if [ "$(helm list release-name | grep release-name | cut -f 2)" -gt "$REVISION"  ]
then
    REVISION=$(helm list release-name | grep release-name | cut -f 2)
fi
echo $REVISION
#building images
docker build -t gcr.io/$(gcloud config get-value project)/pow:$REVISION ./pow
docker build -t gcr.io/$(gcloud config get-value project)/add:$REVISION ./add
docker build -t gcr.io/$(gcloud config get-value project)/notes:$REVISION ./notes
docker build -t gcr.io/$(gcloud config get-value project)/gateway:$REVISION ./gateway

#pushing images
gcloud docker -- push gcr.io/$(gcloud config get-value project)/pow:$REVISION
gcloud docker -- push gcr.io/$(gcloud config get-value project)/notes:$REVISION
gcloud docker -- push gcr.io/$(gcloud config get-value project)/add:$REVISION
gcloud docker -- push gcr.io/$(gcloud config get-value project)/gateway:$REVISION
#deploy app
helm template ./demo --set project_id=$(gcloud config get-value project)
helm upgrade --install release-name ./demo

#updating image
kubectl set image deployment/gateway-deployment gateway=gcr.io/$(gcloud config get-value project)/gateway:$REVISION
kubectl set image deployment/add-deployment add=gcr.io/$(gcloud config get-value project)/add:$REVISION
kubectl set image deployment/pow-deployment pow=gcr.io/$(gcloud config get-value project)/pow:$REVISION
kubectl set image deployment/notes-deployment notes=gcr.io/$(gcloud config get-value project)/notes:$REVISION


from flask import Flask, jsonify
import json
from werkzeug.exceptions import NotFound


app = Flask(__name__)



@app.route("/", methods=['GET'])
def hello():
    return jsonify({
        "uri": "/",
        "subresource_uris": {
            "pow": "/pow/<x>/<y>"
        }
    })


@app.route("/pow/<int:x>/<int:y>", methods=['GET'])
def pow(x, y):
    return jsonify(x ** y)

if __name__ == "__main__":
    app.run(host= "0.0.0.0", port=8082, debug=True)


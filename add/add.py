from flask import Flask, jsonify
import json
from werkzeug.exceptions import NotFound


app = Flask(__name__)



@app.route("/", methods=['GET'])
def hello():
    return jsonify({
        "uri": "/",
        "subresource_uris": {
            "add": "/add/<x>/<y>"
        }
    })


@app.route("/add/<int:x>/<int:y>", methods=['GET'])
def add(x, y):
    return jsonify(x + y)

if __name__ == "__main__":
    app.run(host= "0.0.0.0", port=8081, debug=True)


import sqlite3
import os

if not os.path.isfile('./notes/database.db'):
    db = sqlite3.connect(os.path.dirname(os.path.abspath(__file__)) + '/notes/database.db', timeout = 10)

    cur = db.cursor()

    cur.execute("CREATE TABLE notes (id VARCHAR(100), text VARCHAR(255), date VARCHAR(100))")
    cur.execute("INSERT INTO notes (id, text, date) VALUES('1', 'This is my first note', '01-03-2019')")
    cur.execute("INSERT INTO notes (id, text, date) VALUES('2', 'This is my second note', '01-03-2019')")
    db.commit()
else:
    print('example database is already created')